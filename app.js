const cors = require("cors");
 const express = require('express');
 const mongoose = require('mongoose');

 require("dotenv").config();

 const app = express();
 const port = process.env.PORT || 5000;

//  const corsOptions = {
//   origin: process.env.CLIENT_URL,
//   optionsSuccessStatus: 200,
// };


 app.use(express.json());
 app.use(cors());

 const savePayments = require("./routes/savePayment");



 app.use("/api", savePayments);




mongoose
  .connect(process.env.DATABASE, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .then(() => console.log("Database connection established!"))
  .catch((error) => console.log("Database connection failed!", error));



app.listen(port, () => {
  console.log(
    `API is listening at port: ${port} on '${process.env.NODE_ENV}' environment!`
  );
});