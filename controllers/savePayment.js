const paymentDetails = require("../models/paymentDetails");

const stripe = require("stripe")(
  "sk_test_51ILsHsCO1T5XCvpOdPGnxMUbRZxF3KyiLMwa8cZg1nMoaOEhrUqJVyDvCTIg05PjSetOMjzNC9RZcb6O8ejrvdHo00yAQNpwfn"
);
exports.SavePayments = (req, res) => {
  var paymentdetail = new paymentDetails({
    Name: req.body.name,
    Email: req.body.email,
    Address: req.body.address,
    City: req.body.city,
    State: req.body.state,
    Zip: req.body.zip,
    Amount: req.body.amount,
    NameOnCard: req.body.nameoncard,
    CreditCardNumber: req.body.cardnumber,
    Category: req.body.category,
    Donationtype: req.body.Donationtype,
  });
  paymentdetail.save(function (err) {
    if (err) {
      res.status(400).send("Something went wrong!");
    }
    if (!err) {
      res.send("ok");
    }
  });
};

exports.getDetails = (req, res) => {
  paymentDetails
    .aggregate([
      {
        $group: {
          _id: "$Category",
          sum: { $sum: "$Amount" },
          // count: { $sum: 1 }
        },
      },
    ])
    .then((details) => {
      if (!details) {
        return res.status(404).send({
          message: "Details not found ",
        });
      }

      res.send(details);
    })
    .catch((err) => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "Details not found",
        });
      }
      return res.status(500).send({
        message: "Error retrieving answer",
      });
    });
};

// const paymentIntent = await stripe.paymentIntents.create({
//   amount: 1000,
//   currency: 'usd',
//   payment_method_types: ['card'],
//   receipt_email: 'debit.beckham@gmail.com',
// });
exports.createPayment = (req, res) => {
  stripe.customers
    .create({
      email: "debit.beckham@gmail.com",
      // source: req.body.stripeToken,
      name: "Debit Paudel",
      address: {
        line1: "TC 9/4 Old MES colony",
        postal_code: "452331",
        city: "Bharatpur",
        state: "Bagamati",
        country: "Nepal",
      },
    })
    .then((customer) => {
      stripe.charges.create({
        amount: 100,
        description: "Web Development Product",
        currency: "USD",
        customer: customer.id,
      });
    })
    .then((charge) => {
      res.send("Success"); // If no error occurs
    })
    .catch((err) => {
      res.send(err); // If some error occurs
    });
};
// const card = await stripe.customers.createSource(   'cus_K08jqorvJ7FdVd',   {source: 'tok_visa'} );
exports.createPayments = async (req, res) => {
  // stripe.charges.create({
  //   amount: 1000, // in cents,
  //   currency: 'usd',
  //   customer:'cus_K09oXMS658YEAK',
  //   // source: 'STRIPE_TOKEN_FROM_CLIENT',
  //   description: 'Any description about the payment',
  //   metadata:{
  //       key: "123" // any meta-data you want to store
  //   }
  // }, (err, charge) => {
  //   if(err) {
  //      console.log(err);
  //   } else {
  //      console.log(charge);
  //   }
  // })

  const token = req.body.stripeToken;
  const amount = req.body.amount; // Using Express

  try {
    const response = await stripe.charges.create({
      amount: amount * 100,
      currency: "usd",
      description: "The Yoga Foundation",
      source: token,
    });
    if (response) {
      res.status(200).send({
        message: "Payment Successful",
      });
    }
  } catch (e) {
    res.status(400).send({
      message: e.message,
    });
  }
  // stripe.paymentIntents.create({
  //   amount: 1099,
  //   currency: 'USD',
  //   payment_method_types: ['card']
  // }, (err, charge) => {
  //   if(err) {
  //      console.log(err);
  //   } else {
  //      console.log(charge);
  //      stripe.confirmCardPayment(clientSecret, {
  //       payment_method: {
  //         card: card,
  //         billing_details: {
  //           name: 'Jenny Rosen'
  //         }
  //       }
  //     }).then(function(result) {
  //       if (result.error) {
  //         // Show error to your customer (e.g., insufficient funds)
  //         console.log(result.error.message);
  //       } else {
  //         // The payment has been processed!
  //         if (result.paymentIntent.status === 'succeeded') {
  //           // Show a success message to your customer
  //           // There's a risk of the customer closing the window before callback
  //           // execution. Set up a webhook or plugin to listen for the
  //           // payment_intent.succeeded event that handles any business critical
  //           // post-payment actions.
  //         }
  //       }
  //     });
  //      res.send(charge)
  //   }
  // })
};
