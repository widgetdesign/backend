const express = require("express");

const router = express.Router();
const {
  SavePayments,
  getDetails,
  createPayments,
} = require("../controllers/savePayment");

router.post("/savePayment", SavePayments);
router.get("/getDetails", getDetails);
router.post("/stripePayment", createPayments);

module.exports = router;
