const mongoose = require('mongoose');

const PaymentDetailsSchema =  mongoose.Schema({
   Name:String,
   Email:String,
   Address:String,
   City:String,
   State:String,
   Zip:String,
   Amount:Number,
   Remarks:String,
   NameOnCard:String,
   CreditCardNumber:String,
   Category:String,
   Donationtype:String
   
  
});

module.exports = mongoose.model('PaymentDetails', PaymentDetailsSchema);